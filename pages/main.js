const countriesList = document.getElementById("countries");
let countries;
countriesList.addEventListener("change", newCountrySelection);

function newCountrySelection(event) {
  displayCountryInfo(event.target.value);
}

fetch("https://restcountries.eu/rest/v2/all")
.then(res => res.json())
.then(data => initialize(data))
.catch(err => console.log("Error:", err));

function initialize(countriesData) {
  countries = countriesData;
  let options = "";
  countries.forEach(country => options+=`<option value="${country.alpha3Code}">${country.name}</option>`);
  countriesList.innerHTML = options;
  countriesList.selectedIndex = 104;
  displayCountryInfo(countriesList[countriesList.selectedIndex].value);
}

function displayCountryInfo(countryByAlpha3Code) {
  const countryData = countries.find(country => country.alpha3Code === countryByAlpha3Code);
  var borders = countryData.borders.map(code => [code, countries.find(count => count.alpha3Code === code).flag]);
  document.querySelector("#flag-container img").src = countryData.flag;
  document.querySelector("#flag-container img").alt = `Flag of ${countryData.name}`;
  document.querySelector("#flag-container a").href = `https://www.google.com/maps/place/${countryData.name}`;
  document.getElementById("nativeName").innerHTML = countryData.nativeName;
  document.getElementById("capital").innerHTML = countryData.capital;
  document.getElementById("population").innerHTML = countryData.population.toLocaleString("en-US");
  document.getElementById("region").innerHTML = countryData.region;
  document.getElementById("subregion").innerHTML = countryData.subregion;
  document.getElementById("area").innerHTML = `${countryData.area.toLocaleString("en-US")} Km<sup>2</sup>`;
  document.getElementById("dialing-code").innerHTML = `+${countryData.callingCodes[0]}`;
  document.getElementById("languages").innerHTML = countryData.languages.filter(c => c.name).map(c => `${c.name}`).join(", ");
  document.getElementById("currencies").innerHTML = countryData.currencies.filter(c => c.name).map(c => `${c.name} (${c.code})`).join(", ");
  document.getElementById("timezone").innerHTML = countryData.timezones[0];
  var div = document.getElementById("border");
  div.innerHTML = "";
  for(let i=0;i<borders.length;i++)
  {
    var imagem=document.createElement("img");
    imagem.src=borders[i][1];
    imagem.alt=borders[i][0];
    div.appendChild(imagem);
  }
  console.log(countries);
}